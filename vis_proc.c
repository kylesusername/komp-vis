/*
Copyright © 2015 Kye Dominguez

This file is part of vis_proc.

vis_proc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vis_proc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with vis_proc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <stdarg.h>

#include <sys/ioctl.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <sys/mman.h>

#include <png.h>

#include <math.h>

#include "vis_proc.h"

/* 
Outline:

general functions
webcam capture
static image interpretation
algorithms
constraint adjustments
blob initial grouping
blob filtering
blob serialization and transmission
*/

void err_log(const char *s, ...)
{
	va_list args;
	va_start(args, s);
	vfprintf(stderr, s, args);
	va_end(args);
}

inline void sfree(void *ptr)
{
	if(ptr != NULL) {
		free(ptr);
	}
}

/*
Begin webcam capture
*/
inline int xioctl(const int fh, const unsigned long request, const void *arg)
{
	int r;
	do {
		r = ioctl(fh, request, arg);
	} while(r == -1 && errno == EINTR);
	return r;
}

int webcam_open(wcam_t *cam, const char *path)
{
	struct v4l2_capability cap;
	int fd;

	if(cam == NULL || cam -> frame != NULL) {
		return -1;
	}

	cam -> frame = malloc(sizeof(*(cam -> frame)));

	fd = open(path, O_RDWR);

	if(fd == -1) {
		err_log("Failed to open %s. ", path);
		switch(errno) {
			case EACCES:
				err_log("Access to the file is not allowed.\n");
				break;
			case ENOENT:
				err_log("No such file or directory\n");
				sfree(cam -> frame);
				return -2;
				break;
			case EFAULT:
				err_log("Pathname points outside of accessable address space\n");
				break;
			case EISDIR:
				err_log("Pathname is a director\n");
				break;
			case ELOOP:
				err_log("Too many simlinks were encountered resolving pathname\n");
				break;
			case ENAMETOOLONG:
				err_log("Filename was too long\n");
				break;
			case ENODEV:
				err_log("Refers to a device that doesn't exist\n");
				break;
			default:
				err_log("Something cray went wrong. Figure it out: %d\n", errno);
				break;
		}
		sfree(cam -> frame);
		return -3;
	}

	if(xioctl(fd, VIDIOC_QUERYCAP, &cap) == -1) {
		err_log("%s is not a valid v4l2 device\n", path);
		return -4;
	}

	if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		err_log("%s is not a capture device\n", path);
		return -5;
	}
	
	cam -> fd = fd;
	cam -> path= strdup(path);
	cam -> frame -> start = NULL;
	cam -> frame -> length = 0;
	cam -> nbuffs = 0;
	cam -> buffs = NULL;

	return 0;
}

int webcam_close(wcam_t *cam)
{
	uint32_t i;
	/* free and clean up */
	sfree(cam -> frame -> start);
	sfree(cam -> frame);
	sfree(cam -> path);
	for(i = 0; i < (cam -> nbuffs); i++) {
		munmap(cam -> buffs[i].start, cam -> buffs[i].length);
	}
	sfree(cam -> buffs);
	sfree(cam);
	return 0;
}

int webcam_resize(wcam_t *cam, const int width, const int height)
{
	size_t i;
	struct v4l2_format *fmt;
	struct v4l2_buffer *buf;
	struct v4l2_requestbuffers *req;

	if(cam == NULL) {
		return -1;
	}

	fmt = calloc(1, sizeof(*fmt));

	fmt -> type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt -> fmt.pix.width = width;
	fmt -> fmt.pix.height = height;
	fmt -> fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt -> fmt.pix.colorspace = V4L2_COLORSPACE_REC709;

	xioctl(cam -> fd, VIDIOC_S_FMT, fmt);

	cam -> width = fmt -> fmt.pix.width;
	cam -> height = fmt -> fmt.pix.width;
	cam -> colorspace = fmt -> fmt.pix.width;

	sfree(fmt);

	if(cam -> buffs != NULL) {
		for(i = 0; i < cam -> nbuffs; ++i) {
			munmap(cam -> buffs[i].start, cam -> buffs[i].length);
		}
		sfree(cam -> buffs);
	}

	req = calloc(1, sizeof(*req));

	req -> count = 4;
	req -> type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req -> memory = V4L2_MEMORY_MMAP;

	if(xioctl(cam -> fd, VIDIOC_REQBUFS, &req) == -1) {
		switch(errno) {
			case EINVAL:
				err_log("%s does not support mmap\n", cam -> path);
				return -1;
				break;
			default:
				err_log("Unknown error with VIDIOC_REQBUFS\n");
				return -1;
				break;
		}
	}

	if(req -> count < 2) {
		err_log("Insufficient memory on %s\n", cam -> path);
		return -2;
	}

	cam -> nbuffs = req -> count;
	cam -> buffs = calloc(cam -> nbuffs, sizeof(*(cam -> buffs)));

	if(cam -> buffs == NULL) {
		return -3;
	}

	sfree(req);

	buf = malloc(sizeof(*buf));

	for(i = 0; i < cam -> nbuffs; ++i) {
		memset(buf, 0, sizeof(*buf));
		buf -> type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf -> memory = V4L2_MEMORY_MMAP;
		buf -> index = i;

		if(xioctl(cam -> fd, VIDIOC_QUERYBUF, buf) == -1) {
			err_log("Could not query the buffers on %s\n", cam -> path);
			return -4;
		}

		cam -> buffs[i].length = buf -> length;
		cam -> buffs[i].start = mmap(NULL, buf -> length, PROT_READ | PROT_WRITE, \
			MAP_SHARED, cam -> fd, buf -> m.offset);

		if(cam -> buffs[i].start == MAP_FAILED) {
			err_log("mmap failed.\n");
			return -5;
		}
	}

	sfree(buf);

	return 0;
}

/*
End webcam capture
*/

/*
Begin static image conversion
*/
int png_open(const char *path, png_t *png)
{
	int status;
	size_t i;
	png_byte *header;

	FILE *fp;

	if(path == NULL || png == NULL) {
		return -1;
	}
	
	header = calloc(8, sizeof(*header));

	fp = fopen(path, "rb");

	if(!fp) {
		err_log("Could not open file at %s\n", path);
		sfree(header);
		return -2;
	}

	status = fread(header, 1, 8, fp);
	if(status != 8) {
		err_log("Reached EOF unexpectedly on %s\n", path);
		sfree(header);
		fclose(fp);
		return -3;
	}

	if(png_sig_cmp(header, 0, 8)) {
		err_log("%s is not a PNG\n", path);
		sfree(header);
		fclose(fp);
		return -5;
	}

	png -> path = strdup(path);
	
	png -> ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if(png -> ptr == NULL) {
		err_log("failed creating png -> ptr\n");
		sfree(header);
		fclose(fp);
		sfree(png -> path);
		return -6;
	}

	png -> iptr = png_create_info_struct(png -> ptr);

	if(png -> iptr == NULL) {
		err_log("failed creating png -> iptr\n");
		sfree(header);
		fclose(fp);
		sfree(png -> path);
		png_destroy_read_struct(&(png -> ptr), NULL, NULL);
		return -7;
	}

	setjmp(png_jmpbuf(png -> ptr));

	png_init_io(png -> ptr, fp);
	png_set_sig_bytes(png -> ptr, 8);

	png_read_info(png -> ptr, png -> iptr);
	
	png -> w = png_get_image_width(png -> ptr, png -> iptr);
	png -> h = png_get_image_height(png -> ptr, png -> iptr);
	png_read_update_info(png -> ptr, png -> iptr);

	setjmp(png_jmpbuf(png -> ptr));

	png -> rptr = (png_byte**) malloc(sizeof(*(png -> rptr)) * (png -> h));
	
	for(i = 0; i < (png -> h); ++i) {
		png -> rptr[i] = (png_byte*) malloc(png_get_rowbytes(png -> ptr, png -> iptr));
	}

	png_read_image(png -> ptr, png -> rptr);

	fclose(fp);
	sfree(header);

	return 0;
}

int png_close(png_t *png)
{
	size_t i;
	if(png == NULL) {
		return -1;
	}

	sfree(png -> path);

	for(i = 0; i < png -> h; ++i) {
		sfree(png -> rptr[i]);
	}

	png_destroy_read_struct(&(png -> ptr), &(png -> iptr), NULL);

	sfree(png -> rptr);
	sfree(png);

	return 0;
}

int png_conv_rgb(const png_t *png, rgb_arr_t *rgb)
{
	size_t w, h;
	uint8_t offset;

	if(png == NULL || rgb == NULL) {
		return -1;
	}

	if(png -> w > IMG_WIDTH || png -> h > IMG_HEIGHT) {
		err_log("png %s is too large for the thingy\n", png -> path);
		return -2;
	}

	switch(png_get_color_type(png -> ptr, png -> iptr)) {
		case PNG_COLOR_TYPE_RGB:
			offset = 3;
			break;
		case PNG_COLOR_TYPE_RGBA:
			offset = 4;
			break;
		default:
			err_log("PNG %s is not in the right colorspace\n", png -> path);
			return -1;
			break;
	}

	for(h = 0; h < png -> h; ++h) {
		png_byte* r = png -> rptr[h];
		for(w = 0; w < png -> w; ++w) {
			png_byte* ptr = &(r[w * offset]);
			rgb -> px[w][h].r = ptr[0];
			rgb -> px[w][h].g = ptr[1];
			rgb -> px[w][h].b = ptr[2];
		}
	}

	return 0;
}

/*
End static image conversion
*/

/*
Begin algorithms
*/
float rgb_diff(const rgb_t rgb0, const rgb_t rgb1)
{
	int rmean = (rgb0.r + rgb1.r) / 2;
	int r = rgb0.r - rgb1.r;
	int g = rgb0.g - rgb1.g;
	int b = rgb0.b - rgb1.b;
	return sqrtf((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
}

inline rgb_eval_diff(const rgb_t rgb0, const rgb_t rgb1, int diff) {
	return rgb_diff(rgb0, rgb1) < diff;
}
/*
End algorithms
*/

/*
Begin constraint adjustments
*/

/*
End constraint adjustments
*/

/*
Begin initial blob grouping
*/

/*
Iterative-ly finds blobs in *rgb, saves and inits them to a NULL blobs_t
*/
int lift_blobs(rgb_arr_t *rgb_arr, blobs_t *blobs)
{
	if(rgb_arr == NULL || blosb == NULL) {
		return -1;
	}

	int i, j, k;
	int **bmap;
	bmap = calloc(IMG_WIDTH, sizeof(*bmap));
	for(i = 0; i < IMG_WIDTH; ++i) {
		bmap[i] = calloc(IMG_HEIGHT, sizeof(*(*bmap)));
	}

	for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++i) {
			
		}
	}
}
/*
End initial blob grouping
*/

/*
Begin blob filtering
*/

/*
0 == not within
1 == within
*/
inline int thresh_rgb(const rgb_t src, const rgb_t min, const rgb_t max)
{
	return (src.r >= min.r && src.r <= max.r && \
			src.g >= min.g && src.g <= max.g && \
			src.b >= min.b && src.b <= max.b);
}

inline int thresh_hsv(const hsv_t src, const hsv_t min, const hsv_t max)
{
	return (src.h >= min.h && src.h <= max.h && \
			src.s >= min.s && src.h <= max.s && \
			src.v >= min.v && src.v <= max.v);

}

inline int thresh_int(const int src, const int min, const int max)
{
	return(src >= min && src <= max);
}

int filter_blobs(const blobs_t *blobs, const filter_t *filter, blobs_t *ret)
{
	size_t i;
	size_t retc = 0;


	if(blobs == NULL || filter == NULL || ret == NULL) {
		err_log("uninitialized variables in filter_blobs\n");
		return -1;
	}

	/*ret = realloc(sizeof(*ret) + (blobs -> count * sizeof(*(ret -> blob))));*/
	ret -> blob = malloc((blobs -> count) * sizeof(*(ret -> blob)));

	for(i = 0; i < blobs -> count; i++) {
		if( thresh_rgb(blobs -> blob[i].rgb_avg, filter -> rgb_min, filter -> rgb_max) && \
			thresh_hsv(blobs -> blob[i].hsv_avg, filter -> hsv_min, filter -> hsv_max) && \
			thresh_int(blobs -> blob[i].size, filter -> size_min, filter -> size_max) && \
			thresh_int(blobs -> blob[i].x, filter -> x_min, filter -> x_max) && \
			thresh_int(blobs -> blob[i].y, filter -> y_min, filter -> y_max) )
		{
			memcpy(&(ret -> blob[retc++]), &(blobs -> blob[i]), sizeof(*(blobs -> blob)));
		}
	}

	/*ret = realloc(ret, sizeof(*ret) + (retc * sizeof(*ret -> blob)));*/
	ret -> blob = realloc(ret -> blob, retc * sizeof(*(ret -> blob)));
	if(ret == NULL) {
		err_log("error reallocing at filter_blobs\n");
		sfree(ret);
		return -2;
	}
	ret -> count = retc;
	return 0;
}

int alanyze_bmaps(const blobs_t *blobs)
{
	int i, j;
	if(blobs == NULL || blobs -> blob == NULL) {
		return -1;
	}
	
	for(i = 0; i < blobs -> blob -> bmap.width; ++i) {
		for(j = 0; j < blobs -> blob -> bmap.height; ++j) {
			/**/;
		}
	}
	return 0;
}
/*
End blob filtering
*/

/*
Begin blob serialization and transmission
*/

/*
End blob serialization and transmission
*/

int main(void)
{
	return 0;
}
