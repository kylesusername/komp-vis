#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

typedef struct {
	int size;
	short x, y;
	char type;
} cat_blobs_t;

typedef struct {
	uint8_t count, data0, data1, data2;
	cat_blobs_t *blobs;
} data_t;

void CatBlobsToH(cat_blobs_t *src) 
{
	src -> size = ntohl(src -> size);
	src -> x = ntohs(src -> x);
	src -> y = ntohs(src -> y);
}

data_t *DeserializeData(unsigned char *buffer, size_t *length) 
{
	int i = 0;
	if(*length >= 4) {
		data_t *recvData = calloc(1U, sizeof(*recvData));
		recvData -> count = buffer[0];
		cat_blobs_t *blobs = calloc(recvData -> count, sizeof(*blobs));
		memcpy(blobs, buffer + 4, (recvData -> count) * sizeof(*blobs));
		for(i = 0; i < recvData -> count; i++) {
			CatBlobsToH(blobs + i);
		}
		recvData -> blobs = blobs;
		return recvData;
	} else
		return NULL;
}

int Destroy(void *source) 
{
	if(source == NULL) {
		return 1;
	}

	free(source);
	
	return 0;
}

int main()
{
	int sockfd, i;
	struct sockaddr_in servaddr;
	size_t *len;

	unsigned char *buffer = calloc(1000U, sizeof(unsigned char));

	sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	memset(&servaddr, 0, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	servaddr.sin_port = htons(1884);
	bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	
	recvfrom(sockfd, buffer, 1000, 0, (void *) NULL, (socklen_t *) len);

	data_t *data = DeserializeData(buffer, len);
	
	for(i = 0; i < data -> count; i++) {
		printf("count: %d, size: %d, x: %d, y: %d, type: %c\n", data -> count, (data -> blobs + i) -> size, (data -> blobs + i) -> x, (data -> blobs + i) -> y, (data -> blobs + i) -> type);
	}
}
