#include <stdio.h>
#include <math.h>
#include <stdint.h>

typedef struct rgbColor
{
	uint8_t R;
	uint8_t G;
	uint8_t B;
} rgb_color;

float calcDiff(rgb_color r0, rgb_color r1)
{
	int rmean= (r0.R + r1.R) / 2;
	int r = r0.R - r1.R;
	int g = r0.G - r1.G;
	int b = r0.B - r1.B;
	printf("%d\n", (((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8));
	return sqrt((((512+rmean)*r*r)>>8) + 4*g*g + (((767-rmean)*b*b)>>8));
}

int main()
{
	rgb_color color0;
	rgb_color color1;

	color0.R = 6
;	color0.G = 26
;	color0.B = 17
;	color1.R = 5
;	color1.G = 20
;	color1.B = 14
;
	printf("diff: %f\n", calcDiff(color0, color1));
	return 0;
}
