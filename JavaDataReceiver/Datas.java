/*
 * A datas data structure that mirrors this C one:
 * struct datas {
 * 		char count;
 * 		char data0;
 * 		char data1;
 * 		char data2;
 * 		struct blobby *blobs;
 * };
 */
public class Datas {
	public int count;
	public int data0;
	public int data1;
	public int data2;
	public Blob[] blobs;
	
	//constructor
	public Datas(int count, int data0, int data1, int data2) {
		this.count = count;
		this.data0 = data0;
		this.data1 = data1;
		this.data2 = data2;
		blobs = new Blob[count];
	}
}
