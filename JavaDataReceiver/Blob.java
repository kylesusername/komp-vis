/*
 * a blob data structure that mirrors the C one which is:
 * struct blobby {
 * 		int size;
 * 		short avgX;
 * 		short avgY;
 * 		char type;
 * 		char data0;
 * 		char data1;
 * 		char data2;
 * };
 * and takes up 12 bytes. 
 * The java one is bloat, as all data types that aren't longs are 32 bytes. 
 */
public class Blob {
	public int size;
	public short avgX;
	public short avgY;
	public char type;
	public int data0;
	public int data1;
	public int data2;
	
	//constructor
	public Blob(int size, short avgX, short avgY, char type, int data0, int data1, int data2){
		this.size = size;
		this.avgX = avgX;
		this.avgY = avgY;
		this.type = type;
		this.data0 = data0;
		this.data1 = data1;
		this.data2 = data2;
	}
}