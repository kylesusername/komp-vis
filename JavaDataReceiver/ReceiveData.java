import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;

public class ReceiveData {

	public static void main(String[] args)
	{
		//declare a byte buffer and a UDP packet
		byte[] buffer;			
		DatagramPacket packet = null;
		
		try {
			int port = 1884;

			// Create a socket to listen on the port.
			DatagramSocket dsocket = new DatagramSocket(port);

			// Create a buffer to read datagrams into.
			buffer = new byte[1000];

			// Create a packet to receive data into the buffer
			packet = new DatagramPacket(buffer, buffer.length);

			// Wait to receive a datagram
			dsocket.receive(packet);
			dsocket.close();
		} catch (Exception e) {
			System.err.println(e);
		}
		//make a ByteBuffer from the packet
		ByteBuffer bBuf = ByteBuffer.wrap(packet.getData());
		
		//convert the ByteBuffer and store it into a Datas datastructure
		Datas data = ReceiveDatas(bBuf);
	}
	//convert a bBuf to a Datas datastructure
	public static Datas ReceiveDatas(ByteBuffer bBuf) {
		//get the count from the first byte received
		int count = bBuf.get(0);
		int current;
		//make a datas datastructure and initialize the blobs array with the size of count
		Datas data = new Datas(count, bBuf.get(1), bBuf.get(2), bBuf.get(3));
		//loop through the array
		for(int i = 0; i < count; i++) {
			current = 4 + (i * 12);
			//set the blob at position i to the bytebuffer from the offset
			data.blobs[i] = ReceiveBlob(bBuf, current);	
		}
		
		return data;
	}
	//Receive a blob with offset i from the bytebuffer bBuf
	public static Blob ReceiveBlob(ByteBuffer bBuf, int i){
		return new Blob(bBuf.getInt(0 + i), bBuf.getShort(4 + i), bBuf.getShort(6 + i), (char) bBuf.get(8 + i), (int) bBuf.get(9 + i), (int) bBuf.get(10 + i), (int) bBuf.get(11 + i));
	}
}