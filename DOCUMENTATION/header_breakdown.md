Here is the header from vis_proc. Outside of the `code` is the rationale

`#ifndef _STDLIB_H`
`	#include <stdlib.h>`
`#endif`
need stdlib.h for defined widths
``
`#define IMG_WIDTH	4`
`#define IMG_HEIGHT	4`
image dimensions, helps to reduce redundant code
`#define WEBCAM_FSPATH "/dev/video0"`
`#define IP_ADDRESS "127.0.0.1"`
`#define PORT 1884`
``
`typedef u_int8_t uint8_t;`
`typedef u_int16_t uint16_t;`
`typedef u_int32_t uint32_t;`
`typedef u_int64_t uint64_t;`
changes the weird names to standard names
`typedef struct rgb_color`
`{`
`	uint8_t r;`
`	uint8_t g;`
`	uint8_t b;`
`} rgb_t;`
values will never be <0 or >255
``
`typedef struct hsv_color`
`{`
`	uint16_t h;`
`	uint8_t  s;`
`	uint8_t  v;`
`} hsv_t;`
only h is >255, the others are <100
``
`typedef struct rgb_array`
`{`
`	rgb_t px[IMG_WIDTH][IMG_HEIGHT];`
`} rgb_arr_t;`
better defines both the bitmap for the frame, and that px refers to a single pixel
``
`typedef struct bitmap`
`{`
`	uint8_t bit[IMG_WIDTH][IMG_HEIGHT];`
`} bmap_t;`
a bitmap that should have only one value, but it has to be byte-aligned
``
`typedef struct blob`
`{`
`	bmap_t bmap;`
`	rgb_t rgb_avg;`
`	hsv_t hsv_avg;`
`	int16_t x;`
`	int16_t y;`
`	uint32_t size;`
`	uint8_t type;`
`} blob_t;`
Contains a bitmap for the shape, the color in both rgb and hsv (one may be NULL), the x, the y, the size and an enumerated type.
``
`typedef struct blobs`
`{`
`	blob_t *blob;`
`	size_t count;`
`} blobs_t;`
Makes it so that the number of blobs alloc'd stays with it's pointer
``
`typedef struct typed_blob`
`{`
`	size_t size;`
`	int16_t x;`
`	int16_t y;`
`	uint8_t type;`
`} tblob_t;`
Once it has been classified, there is no need to carry around those extra fields
``
`typedef struct typed_blobs`
`{`
`	tblob_t *blob;`
`	size_t count;`
`} tblobs_t;`
same reason as blobs_t
``
`typedef struct blob_filter`
`{`
`	rgb_t rgb_min, rgb_max;`
`	hsv_t hsv_min, hsv_max;`
`	int16_t x_min, x_max;`
`	int16_t y_min, y_max;`
`	size_t size_min, size_max;`
`} filter_t;`
Has a min and max for all of the fields in blobs_t
``
`typedef struct tblob_filter`
`{`
`	size_t size_min, size_max;`
`	int16_t x_min, x_max;`
`	int16_t y_min, y_max;`
`	uint8_t type;`
`} tfilter_t;`
Has a min and a max for all of the fields in tblobs_t
``
`typedef struct w_buffer`
`{`
`	uint8_t *start;`
`	size_t length;`
`} wbuff_t;`
buffer with a pointer ot the buffer, and the length of the buffer
``
`typedef struct webcam`
`{`
`	wbuff_t *frame;`
`	wbuff_t *buffs;`
`	char *path;`
`	int fd;`
`	uint32_t nbuffs;`
`	uint32_t width;`
`	uint32_t height;`
`	uint32_t colorspace;`
`} wcam_t;`
Has the current frame, a pointer to an array of the buffers, the path to the webcam, the file descriptor of the webcam ,the number of buffers, the width and height of the image, and the colorspace.
``
`typedef struct png_image`
`{`
`	char *path;`
`	png_uint_32 w;`
`	png_uint_32 h;`
`	png_struct *ptr;`
`	png_info *iptr;`
`	png_byte **rptr;`
`} png_t;`
Has all of the libpng values in a handy place to reduce the number of variables passed
