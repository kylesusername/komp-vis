/*
Copyright © 2015 Kyl Dominguez

This file is part of vis_proc.

vis_proc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vis_proc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with vis_proc.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _STDLIB_H
	#include <stdlib.h>
#endif

#define IMG_WIDTH	320
#define IMG_HEIGHT	240
#define WEBCAM_FSPATH "/dev/video0"

typedef struct rgb_color
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb_t;

typedef struct hsv_color
{
	uint16_t h;
	uint8_t  s;
	uint8_t  v;
} hsv_t;

typedef struct rgb_array
{
	rgb_t px[IMG_WIDTH][IMG_HEIGHT];
} rgb_arr_t;

typedef struct int_array
{
	int val[IMG_WIDTH][IMG_HEIGHT];
} int_array_t;

typedef struct bitmap
{
	int width;
	int height;
	uint8_t bit[IMG_WIDTH][IMG_HEIGHT];
} bmap_t;

typedef struct bmap_array
{
	int count;
	bmap_t **bmaps;
} bmap_arr_t;

typedef struct blob
{
	bmap_t bmap;
	rgb_t rgb_avg;
	hsv_t hsv_avg;
	int16_t x;
	int16_t y;
	uint32_t size;
	uint8_t type;
} blob_t;

typedef struct blobs
{
	blob_t *blob;
	size_t count;
} blobs_t;

typedef struct typed_blob
{
	size_t size;
	int16_t x;
	int16_t y;
	uint8_t type;
} tblob_t;

typedef struct typed_blobs
{
	tblob_t *blob;
	size_t count;
} tblobs_t;

typedef struct blob_filter
{
	rgb_t rgb_min, rgb_max;
	hsv_t hsv_min, hsv_max;
	int16_t x_min, x_max;
	int16_t y_min, y_max;
	size_t size_min, size_max;
} filter_t;

typedef struct tblob_filter
{
	size_t size_min, size_max;
	int16_t x_min, x_max;
	int16_t y_min, y_max;
	uint8_t type;
} tfilter_t;

typedef struct w_buffer
{
	uint8_t *start;
	size_t length;
} wbuff_t;

typedef struct webcam
{
	wbuff_t *frame;
	wbuff_t *buffs;
	char *path;
	int fd;
	uint32_t nbuffs;
	uint32_t width;
	uint32_t height;
	uint32_t colorspace;
} wcam_t;

typedef struct png_image
{
	char *path;
	png_uint_32 w;
	png_uint_32 h;
	png_struct *ptr;
	png_info *iptr;
	png_byte **rptr;
} png_t;

typedef struct cartesian_point
{
	double x, y, z;
} cartesian_t;

typedef struct spherical_point
{
	double row, theta, phi;
} spherical_t;

typedef struct trapezoid_cartesian
{
	cartesian_t a, b, c, d;
} trap_cart_t;

typedef struct trapezoid_spherical
{
	spherical_t a, b, c, d;
} trap_spher_t;
