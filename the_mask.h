/*
Copyright © 2015 Kyle Dominguez

This file is part of the_mask.

the_mask is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

the_mask is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the_mask.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
This header file has all of the DEFINE constants and struct declarations structure.
*/

/*
	Defines
*/
#define IMG_WIDTH 320
#define IMG_HEIGHT 240
#define THRESH 15

/* bin limit constants */
#define BIN_H_UPPER 160
#define BIN_H_LOWER 110

#define BIN_S_UPPER 101
#define BIN_S_LOWER 50

#define BIN_V_UPPER 20
#define BIN_V_LOWER 1

/* the minimum blob size */
#define BLOB_SIZE_MIN 1000

/* the webcam path to use */
#define WEBCAM_PATH "/dev/video0"

#define WEBCAM_BRIGHTNESS
#define WRITE_FILE

#define IP_ADDRESS "10.18.84.21"
#define PORT 1884

/*
Structures
*/

typedef struct w_buffer
{
	int8_t *start;
	unsigned long int length;
} w_buffer_t;

typedef struct webcam
{
	w_buffer_t frame;
	w_buffer_t *buffers;
	char *name;
	int32_t fd;
	uint32_t nbuffers;
	uint32_t width;
	uint32_t height;
	uint32_t colorspace;
	char formats[16][5];
} webcam_t;

typedef struct rgb_color
{
	uint_fast8_t r, g, b;
} rgb_color_t;

typedef struct hsv_color
{
	uint_fast16_t h;
	uint_fast8_t s, v;
} hsv_color_t;

typedef struct marker_array
{
	int32_t cell[IMG_WIDTH][IMG_HEIGHT];
} marker_array_t;

typedef struct rgb_picture
{
	struct rgb_color color[IMG_WIDTH + 1][IMG_HEIGHT + 1];
} rgb_picture_t;

typedef struct blob_avg
{
	int32_t x_avg;
	int32_t y_avg;
	size_t size;
	uint32_t r_avg;
	uint32_t g_avg;
	uint32_t b_avg;
	unsigned char type;
} blob_avg_t;

typedef struct typed_blob_avg
{
	size_t size;
	int16_t avg_x;
	int16_t avg_y;
	unsigned char type;
} typed_blob_t;

typedef struct blobs_avg
{
	blob_avg_t blobs;
	size_t size;
} blobs_t;

typedef struct typed_blobs
{
	typed_blob_t blobs;
	size_t size;
} tblobs_t;

typedef struct filter_constraints
{
	int16_t x_min, x_max;
	int16_t y_min, y_max;
	size_t size_min, size_max;
	uint8_t roh_min, roh_max;
	uint8_t gos_min, gos_max;
	uint8_t bov_min, bov_max;
} filter_t;

typedef struct blob_data
{
	char count;
	char data_0;
	char data_1;
	char data_2;
	typed_blob_t *blobs;
} data_to_send_t;
