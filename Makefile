CC=clang
CFLAGS=-Wall -g
LDFLAGS+= -lm -lpng
OPTFLAGS=-O2


CFLAGS+=$(OPTFLAGS)

all: vis_proc2016

arm:
	CFLAGS+=-march=armv7-a -mtune=cortex-a8 -mfloat-abi=hard -mfpu=neon
	make vis_proc

vis_proc:

clean:
	rm -f vis_proc2016
