/*
Copyright © 2015 Kyle Dominguez

This file is part of vis_proc.

vis_proc is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

vis_proc is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with vis_proc.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

#include <stdarg.h>

#include <sys/ioctl.h>
#include <errno.h>
#include <linux/videodev2.h>
#include <fcntl.h>
#include <sys/types.h>
#include <string.h>
#include <sys/mman.h>

#include <png.h>

#include <math.h>

#include "vis_proc2016.h"

/* 
Outline:

general functions
webcam capture
static image interpretation
algorithms
frame filtering
shape detection
triangulation
blob serialization and transmission
*/

void err_log(const char *s, ...)
{
	va_list args;
	va_start(args, s);
	vfprintf(stderr, s, args);
	va_end(args);
}

inline void sfree(void *ptr)
{
	if(ptr != NULL) {
		free(ptr);
	}
}

/*
Begin webcam capture
*/

//a convinience function, really. Webcams are a pain in the ass. Will continually try ioctl if the error is EINTR
inline int xioctl(const int fh, const unsigned long request, const void *arg)
{
	int r;
	do {
		r = ioctl(fh, request, arg);
	} while(r == -1 && errno == EINTR);
	return r;
}

//Will initialized the webcam_t struct from the camera at the given path
int webcam_open(wcam_t *cam, const char *path)
{
	struct v4l2_capability cap;
	int fd;

	if(cam == NULL || cam -> frame != NULL) {
		err_log("Passed a webam_t with more than just itself initialized to webcam_open\n");
		return -1;
	}

	cam -> frame = malloc(sizeof(*(cam -> frame)));

	fd = open(path, O_RDWR);

	//many reasons that opening the file may fail, report which one it was
	if(fd == -1) {
		err_log("Failed to open %s. ", path);
		switch(errno) {
			case EACCES:
				err_log("Access to the file is not allowed.\n");
				break;
			case ENOENT:
				err_log("No such file or directory\n");
				sfree(cam -> frame);
				return -2;
				break;
			case EFAULT:
				err_log("Pathname points outside of accessable address space\n");
				break;
			case EISDIR:
				err_log("Pathname is a director\n");
				break;
			case ELOOP:
				err_log("Too many simlinks were encountered resolving pathname\n");
				break;
			case ENAMETOOLONG:
				err_log("Filename was too long\n");
				break;
			case ENODEV:
				err_log("Refers to a device that doesn't exist\n");
				break;
			default:
				err_log("Something cray went wrong. Figure it out: %d\n", errno);
				break;
		}
		sfree(cam -> frame);
		return -3;
	}

	//query the webcam if it is a V4L2 device
	if(xioctl(fd, VIDIOC_QUERYCAP, &cap) == -1) {
		err_log("%s is not a valid v4l2 device\n", path);
		return -4;
	}

	//query the webcam if it is a capture device
	if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		err_log("%s is not a capture device\n", path);
		return -5;
	}
	
	//fill in the struct with the information about the webcam.
	cam -> fd = fd;
	cam -> path = strdup(path);
	cam -> frame -> start = NULL;
	cam -> frame -> length = 0;
	cam -> nbuffs = 0;
	cam -> buffs = NULL;

	return 0;
}

//will clean up the webcam structure and opened files and stuff
int webcam_close(wcam_t *cam)
{
	size_t i;
	/* free and clean up */
	sfree(cam -> frame -> start);
	sfree(cam -> frame);
	sfree(cam -> path);

	for(i = 0; i < (cam -> nbuffs); i++) {
		munmap(cam -> buffs[i].start, cam -> buffs[i].length);
	}

	sfree(cam -> buffs);
	sfree(cam);

	return 0;
}

//will resize the webcam frame and set the colorspace information
int webcam_resize(wcam_t *cam, const int width, const int height)
{
	size_t i;

	if(cam == NULL) {
		return -1;
	}

	//using calloc to zero it
	struct v4l2_format *fmt = calloc(1, sizeof(*fmt));

	//setting the requested colorspace and width and height
	fmt -> type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt -> fmt.pix.width = width;
	fmt -> fmt.pix.height = height;
	fmt -> fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt -> fmt.pix.colorspace = V4L2_COLORSPACE_REC709;

	xioctl(cam -> fd, VIDIOC_S_FMT, fmt);

	//set info in the cam structure about the webcam's info
	cam -> width = fmt -> fmt.pix.width;
	cam -> height = fmt -> fmt.pix.height;
	cam -> colorspace = fmt -> fmt.pix.colorspace;

	sfree(fmt);

	//if the cam structure has had a number of buffered frames allocated, clear them
	if(cam -> buffs != NULL) {
		for(i = 0; i < cam -> nbuffs; ++i) {
			munmap(cam -> buffs[i].start, cam -> buffs[i].length);
		}
		sfree(cam -> buffs);
	}

	//calloc to zero it
	struct v4l2_requestbuffers *req = calloc(1, sizeof(*req));

	//request four buffers
	req -> count = 4;
	req -> type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req -> memory = V4L2_MEMORY_MMAP;


	//make sure that setting the request goes well, if not declare the error
	if(xioctl(cam -> fd, VIDIOC_REQBUFS, &req) == -1) {
		switch(errno) {
			case EINVAL:
				err_log("%s does not support mmap\n", cam -> path);
				return -1;
				break;
			default:
				err_log("Unknown error with VIDIOC_REQBUFS\n");
				return -1;
				break;
		}
	}

	if(req -> count < 2) {
		err_log("Insufficient memory on %s\n", cam -> path);
		return -2;
	}

	cam -> nbuffs = req -> count;
	cam -> buffs = calloc(cam -> nbuffs, sizeof(*(cam -> buffs)));

	if(cam -> buffs == NULL) {
		err_log("Bad juju in the memory allocator in webcam_resize\n");
		return -3;
	}

	sfree(req);

	struct v4l2_buffer *buf = malloc(sizeof(*buf));

	//set the number of buffers in memory
	for(i = 0; i < cam -> nbuffs; ++i) {
		memset(buf, 0, sizeof(*buf));
		buf -> type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf -> memory = V4L2_MEMORY_MMAP;
		buf -> index = i;

		if(xioctl(cam -> fd, VIDIOC_QUERYBUF, buf) == -1) {
			err_log("Could not query the buffers on %s\n", cam -> path);
			return -4;
		}

		cam -> buffs[i].length = buf -> length;
		cam -> buffs[i].start = mmap(NULL, buf -> length, PROT_READ | PROT_WRITE, \
			MAP_SHARED, cam -> fd, buf -> m.offset);

		if(cam -> buffs[i].start == MAP_FAILED) {
			err_log("mmap failed.\n");
			return -5;
		}
	}

	sfree(buf);

	return 0;
}

/*
End webcam capture
*/

/*
Begin static image conversion
*/

/* old PNG code
int png_open(const char *path, png_t *png)
{
	int status;
	size_t i;
	png_byte *header;

	FILE *fp;

	if(path == NULL || png == NULL) {
		return -1;
	}
	
	header = calloc(8, sizeof(*header));

	fp = fopen(path, "rb");

	if(!fp) {
		err_log("Could not open file at %s\n", path);
		sfree(header);
		return -2;
	}

	status = fread(header, 1, 8, fp);
	if(status != 8) {
		err_log("Reached EOF unexpectedly on %s\n", path);
		sfree(header);
		fclose(fp);
		return -3;
	}

	if(png_sig_cmp(header, 0, 8)) {
		err_log("%s is not a PNG\n", path);
		sfree(header);
		fclose(fp);
		return -5;
	}

	png -> path = strdup(path);
	
	png -> ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

	if(png -> ptr == NULL) {
		err_log("failed creating png -> ptr\n");
		sfree(header);
		fclose(fp);
		sfree(png -> path);
		return -6;
	}

	png -> iptr = png_create_info_struct(png -> ptr);

	if(png -> iptr == NULL) {
		err_log("failed creating png -> iptr\n");
		sfree(header);
		fclose(fp);
		sfree(png -> path);
		png_destroy_read_struct(&(png -> ptr), NULL, NULL);
		return -7;
	}

	setjmp(png_jmpbuf(png -> ptr));

	png_init_io(png -> ptr, fp);
	png_set_sig_bytes(png -> ptr, 8);

	png_read_info(png -> ptr, png -> iptr);
	
	png -> w = png_get_image_width(png -> ptr, png -> iptr);
	png -> h = png_get_image_height(png -> ptr, png -> iptr);
	png_read_update_info(png -> ptr, png -> iptr);

	setjmp(png_jmpbuf(png -> ptr));

	png -> rptr = (png_byte**) malloc(sizeof(*(png -> rptr)) * (png -> h));
	
	for(i = 0; i < (png -> h); ++i) {
		png -> rptr[i] = (png_byte*) malloc(png_get_rowbytes(png -> ptr, png -> iptr));
	}

	png_read_image(png -> ptr, png -> rptr);

	fclose(fp);
	sfree(header);

	return 0;
}

int png_close(png_t *png)
{
	size_t i;
	if(png == NULL) {
		return -1;
	}

	sfree(png -> path);

	for(i = 0; i < png -> h; ++i) {
		sfree(png -> rptr[i]);
	}

	png_destroy_read_struct(&(png -> ptr), &(png -> iptr), NULL);

	sfree(png -> rptr);
	sfree(png);

	return 0;
}

int png_conv_rgb(const png_t *png, rgb_arr_t *rgb)
{

	if(png == NULL || rgb == NULL) {
		return -1;
	}

	size_t w, h;
	uint8_t offset;

	if(png -> w > IMG_WIDTH || png -> h > IMG_HEIGHT) {
		err_log("png %s is too large for the thingy\n", png -> path);
		return -2;
	}

	switch(png_get_color_type(png -> ptr, png -> iptr)) {
		case PNG_COLOR_TYPE_RGB:
			offset = 3;
			break;
		case PNG_COLOR_TYPE_RGBA:
			offset = 4;
			break;
		default:
			err_log("PNG %s is not in the right colorspace\n", png -> path);
			return -1;
			break;
	}

	for(h = 0; h < png -> h; ++h) {
		png_byte* r = png -> rptr[h];
		for(w = 0; w < png -> w; ++w) {
			png_byte* ptr = &(r[w * offset]);
			rgb -> px[w][h].r = ptr[0];
			rgb -> px[w][h].g = ptr[1];
			rgb -> px[w][h].b = ptr[2];
		}
	}

	return 0;
}

int png_write(png_t *png, char *path)
{
	FILE *fp = fopen(path, "wb");

	if(!fp) {
		err_log("Failed to open png to write at %s\n", path);
		return -1;
	}

	uint8_t header[8];

	fread(header, 1, 8, fp);
	if(png_sig_cmp(header, 0, 8)) {
		err_log("png %s is not recognized as a png\n", path);
	}

	png -> ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	
	if(png -> ptr == NULL) {
		err_log("Bad pointer allocation at png_write for png->ptr\n");
		return -1;
	}

	png -> iptr = png_create_info_struct(png -> ptr);

	if(png -> iptr == NULL) {
		err_log("Bad pointer allocation at png_write for png->info\n");
		return -1;
	}

	if(setjmp(png_jmpbuf(png -> ptr))) {
		return -1;
		err_log("error with setjmp in png_write\n");
	}

	png_init_io(png -> ptr, fp);

	//header
	if(setjmp(png_jmpbuf(png -> ptr))) {
		return -1;
		err_log("something wrong in png_write\n");
	}

	png_set_IHDR(png -> ptr, png -> iptr, png -> w, png -> h, 3, \
		PNG_COLOR_TYPE_RGB, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, \
		PNG_FILTER_TYPE_BASE);

	png_write_info(png -> ptr, png -> iptr);

}

int png_unconv_rgb(const rgb_arr_t *rgb, png_t *png)
{
	if(png == NULL || rgb == NULL) {
		return -1;
	}

	if(png -> w != IMG_WIDTH || png -> h != IMG_HEIGHT) {
		err_log("png is not good enough\n");
	}

	size_t w, h;
	uint8_t offset;

} */

/* New PNG code */

//will open the png at path and 
int png_open(const char *path, png_t *png)
{
	//verifiying parameters
	if(path == NULL || png == NULL) {
		err_log("Passed uninitlalized pointers to png_open\n");
		return -1;
	}

	int status;

	FILE *fp = fopen(path, "rb");
	if(!fp) {
		err_log("Could not open file at %s in png_open\n", path);
		return -1;
	}

	png -> path = calloc(strlen(path) + 1, sizeof(*(png -> path)));
	png -> path = strcpy(png -> path, path);

	//verifying that the PNG is valid
	unsigned char *header = calloc(8, sizeof(*header));

	status = fread(header, 1, 8, fp);

	if(!status) {
		err_log("Didn't read lol at png_open from %s\n", path);
		free(header);
		return -1;
	}

	if(png_sig_cmp(header, 0, 8)) {
		err_log("%s is not a png\n", path);
		free(header);
		return -1;
	}

	free(header);
	
	//creating the libpng stuff
	png -> ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if(!(png -> ptr)) {
		err_log("Failed to create PNG read struct\n");
		return -1;
	}

	png -> iptr = png_create_info_struct(png -> ptr);
	if(!(png -> iptr)) {
		err_log("Failed to create PNG info struct\n");
		return -1;
	}

	if(setjmp(png_jmpbuf(png -> ptr))) {
		err_log("Failure during init_io\n");
		return -1;
	}

	png_init_io(png -> ptr, fp);
	png_set_sig_bytes(png -> ptr, 8);

	png_read_info(png -> ptr, png -> iptr);

	png -> w = png_get_image_width(png -> ptr, png -> iptr);
	png -> h = png_get_image_height(png -> ptr, png -> iptr);
	
	png_read_update_info(png -> ptr,  png -> iptr);

	//reading the file
	if(setjmp(png_jmpbuf(png -> ptr))) {
		err_log("Failure during the image read\n");
		return -1;
	}
	
	//allocating memory for the contents of the PNG
	png -> rptr = calloc(png -> h, sizeof(**(png -> rptr)));
	
	int i;
	
	size_t rowbytes = png_get_rowbytes(png -> ptr, png -> iptr);
	for(i = 0; i < png -> h; ++i) {
		(png -> rptr)[i] = malloc(rowbytes);
	}

	//writing the image into the memory locations
	png_read_image(png -> ptr, png -> rptr);

	fclose(fp);

	return 0;
}

//will destroy my png_t and all associated data
int png_destroy(png_t *png)
{
	if(png == NULL) {
		err_log("Null pointer passed to png_close\n");
		return -1;
	}
	if(png -> rptr != NULL)  {
		if(*(png -> rptr) != NULL) {
			int i;
			for(i = 0; i < png -> h; ++i) {
				free((png -> rptr)[i]);
			}
		}
		free(png -> rptr);
	}
	
	if(png -> path != NULL) {
		free(png -> path);
	}

	if(png -> ptr != NULL && png -> iptr != NULL) {
		png_destroy_read_struct(&(png -> ptr), &(png -> iptr), NULL);
	}

	free(png);

	return 0;
}

//pass a non-null png and a non-null RGB and it will transfer the data of the PNG to the rgb arr
int png_conv_rgb(const png_t *png, rgb_arr_t *rgb)
{
	if(png == NULL || rgb == NULL) {
		err_log("Passed null pointers too png_conv_rgb\n");
		return -1;
	}

	size_t w, h;
	uint8_t offset;

	if(png -> w > IMG_WIDTH || png -> h > IMG_HEIGHT) {
		err_log("png %s is too large for the RGB array\n", png -> path);
		return -1;
	}

	switch(png_get_color_type(png -> ptr, png -> iptr)) {
		case PNG_COLOR_TYPE_RGB:
			offset = 3;
			break;
		case PNG_COLOR_TYPE_RGBA:
			offset = 4;
			break;
		default:
			err_log("PNG %s is not in the right colorspace\n", png -> path);
			return -1;
			break;
	}

	for(h = 0; h < png -> h; ++h) {
		png_byte* r = png -> rptr[h];
		for(w = 0; w < png -> w; ++w) {
			png_byte* ptr = &(r[w * offset]);
			rgb -> px[w][h].r = ptr[0];
			rgb -> px[w][h].g = ptr[1];
			rgb -> px[w][h].b = ptr[2];
		}
	}

	return 0;
}

int rgb_write_png(const char *path, const rgb_arr_t *rgb)
{
	if(path == NULL || rgb == NULL) {
		err_log("Null pointers passed to rgb_write_png\n");
		return -1;
	}

	int i, w, h;
	png_byte **rptr;

	rptr = calloc(IMG_HEIGHT, sizeof(*rptr));

	if(rptr == NULL) {
		err_log("Memory issue allocating rptr\n");
	}

	for(i = 0; i < IMG_HEIGHT; ++i) {
		rptr[i] = calloc(IMG_WIDTH, sizeof(**(rptr)));
		if(rptr[i] == NULL) {
			err_log("Memory issue allocating parts of rptr\n");
		}
	}

	for(w = 0; w < IMG_WIDTH; ++w) {
		for(h = 0; i < IMG_HEIGHT; ++h) {
			
		}
	}

	return 0;
}

/*
End static image conversion
*/

/*
Begin frame filtering
*/ 

//returns 1 if rgb is not black
int _rgb_notblack(const rgb_t *rgb)
{
	return rgb -> r && rgb -> g && rgb -> b;
}

//will compare two RGB values, and if all parts of rgb is larger than comp, returns 1. Else, returns 0.
inline int _rgb_comp(const rgb_t *rgb, const rgb_t *comp)
{
	if(rgb == NULL || comp == NULL) {
		return -1;
	}

	if(rgb -> r >= comp -> r &&\
	   rgb -> g >= comp -> g &&\
	   rgb -> b >= comp -> b)
	{
		return 1;
	}
	return 0;
}

//will return the HSV colorspace 'value' of the given RGB color thingy.
int _rgb_value(const rgb_t *rgb)
{
	int ret = -1;
	if(rgb == NULL) {
		return ret;
	}
	
	//if R is greater, use R. If G is greater, use G. If B is greater, use B.
	if(rgb -> r >= rgb -> g && rgb -> r >= rgb -> b) {
		ret = rgb -> r;
	} else if(rgb -> g >= rgb -> b) {
		ret = rgb -> b;
	} else {
		ret = rgb -> g;
	}

	//scale it into HSV's 0-100 from RGB's 0-255. Will lose some precision
	return ret * 100 / 255;
}

//Will zero out the pixels with a V less than v_thresh.
int filter_rgb_value(rgb_arr_t *arr, int v_thresh)
{
	if(arr == NULL) {
		err_log("Bad pointer passed at filter_rgb_value\n");
		return -1;
	}

	int i = 0, j = 0;
	rgb_t rgb_zero = {0, 0, 0};

	for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++j) {
			//checking whether or not it is black to potentially save some computation. Who knows if it nets positive.
			if(_rgb_notblack(&(arr -> px[i][j]))) {
				if(_rgb_value(&(arr -> px[i][j])) < v_thresh) {
					arr -> px[i][j] = rgb_zero;
				}
			}
		}
	}

	return 0;
}

//filters out all pixels that are not green. Green things having a maximum of their 'R', 'G', and 'B' being green.
int filter_rgb_green(rgb_arr_t *arr)
{
	if(arr == NULL) {
		err_log("Bad pointer passed to filter_rgb_value\n");
		return -1;
	}

	int i = 0, j = 0;
	rgb_t rgb_zero = {0, 0, 0};
	
	for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++j) {
			//if the g is less than the b or the g is less than the r. Basically goes to that guy's truth thing of !A || !B == A && B or something
			if(arr -> px[i][j].g < arr -> px[i][j].b || arr -> px[i][j].g < arr -> px[i][j].r) {
				arr -> px[i][j] = rgb_zero;
			}
		}
	}

	return 0;
}



//Will zero out the pixels less than the rgb_t thresh.
int filter_rgb_abs(rgb_arr_t *arr, const rgb_t *thresh)
{
	if(arr == NULL || thresh == NULL) {
		return -1;
		err_log("Bad pointers passed to filter_rgb_abs\n");
	}

	int i = 0, j = 0;
	rgb_t rgb_zero = {0, 0, 0};
	for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++j) {
			//again, check if it is black or not to potentially save some computation.
			if(_rgb_notblack(&(arr -> px[i][j]))) {
				if(_rgb_comp(&(arr -> px[i][j]), thresh) > 0) {
					arr -> px[i][j] = rgb_zero;
				}
			}
		}
	}
	return 0;
}
//debugging counting variables
int iters = 0, checks = 0, comps = 0;

//the DFS function that sets finds the size of each mass of like-colored pixels.
void _recursive_detection(int x, int y, rgb_arr_t *arr, int_array_t *intarr, int mval)
{
	//couning variable
	++iters;
	int i, j;
	//set the array of markers to be the value passed into the DFS
	intarr -> val[x][y] = mval;

	//check in the 9 pixels surrounding, and run maybe
	for(i = x - 1; i < x + 2; ++i) {
		for(j = y - 1; j < y + 2; ++j) {
			if(intarr -> val[i][j] == 0) {
				++checks;
				if(_rgb_notblack(&(arr -> px[i][j]))) {
					++comps;
					_recursive_detection(i, j, arr, intarr, mval);
				}
			}
		}
	}
}

int filter_largest_area(rgb_arr_t *arr)
{

	int i = 0, j = 0, count = 0;
	rgb_t rgb_zero = {0, 0, 0};

	typedef struct frames {
		int startx, starty, size;
	} frames_t;

	int_array_t *arrmap = calloc(1, sizeof(*arrmap));
	
	/* new fancy thingy */
	count = 0;
	for(i = 1; i < IMG_WIDTH - 1; ++i) {
		for(j = 1; j < IMG_HEIGHT; ++j) {
			if(arrmap -> val[i][j] == 0) {
				++checks;
				if(_rgb_notblack(&(arr -> px[i][j]))) {
					++comps;
					++count;
					_recursive_detection(i, j, arr, arrmap, count);
				}
			}
		}
	}

	printf("iters: %d, checks: %d, comps: %d\n", iters, checks, comps);

	/*
	//Old iterative one. Is work badly. Am rewrite.h

	//doing the magical algorithm.
	//Basically, it traverses the 2D array, and if it finds like-pixels in any diagonal spot below itself, it will take it as its child and make a beautiful family.
	
	for(j = 0; j < IMG_HEIGHT - 1; ++j) {
		for(i = 1; i < IMG_WIDTH - 1; ++i) {
			if(_rgb_notblack(&(arr -> px[i][j]))) {
				printf("arrmap[%d][%d] = %d. ",i, j, arrmap[i][j]);
				int index;
				if(arrmap[i][j] == 0) {
					printf("incrementing arrmap, ");
					index = ++count;
				} else {
					index = arrmap[i][j];
				}
				arrmap[i][j] = index;
				//down, diagonal down left, diagonal down right
				int k;
				for(k = -1; k < 2; ++k) {
					if(_rgb_notblack(&(arr -> px[i][j]))) {
						arrmap[i + k][j + 1] = index;
					}
				}
			}
		}
	}*/

	printf("count: %d\n", count);
	
	frames_t frames[count];

	for(i = 0; i < count; ++i) {
		frames[i].size = 0;
		frames[i].startx = -1;
		frames[i].starty = -1;
	}
	
	printf("sizes\n");
	//calculate the sizes of the thingies
	for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++j) {
			if(arrmap -> val[i][j] - 1 > count) {
			printf("if thing > other thing\n");
			}
			frames[arrmap -> val[i][j] - 1].size += 1;
			if(frames[arrmap -> val[i][j] - 1].startx == -1 || \
			  frames[arrmap -> val[i][j] - 1].starty == -1) {
				frames[arrmap -> val[i][j] - 1].startx = i;
				frames[arrmap -> val[i][j] - 1].starty = j;
			}
		}
	}

	printf("biggun\n");
	//find the largest thingy.
	int max = 0;
	int max_count = 0;
	for(i = 0; i < count; ++i) {
		if(frames[i].size > max) {
			max = frames[i].size;
			max_count = i;
		}
	}

	printf("max_count: %d, count = %d\n", max_count, count);

	printf("size of largest: %d\n", frames[max_count].size);

	printf("filter'd\n");
	//delete all but the largest thingy.
	for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++j) {
			if(arrmap -> val[i][j] != max_count) {
				arr -> px[i][j] = rgb_zero;
			}
		}
	}

	return 0;
} 

/*
End frame filtering
*/ 

/*
Begin shape detection
*/

int find_trapezoid(rgb_arr_t *arr, trap_cart_t *trap)
{
	if(arr == NULL || trap == NULL) {
		err_log("Passed null things to find_trapezoid\n");
		return -1;
	}


	return 0;
}

/*
End shape detection
*/

/*
Begin blob serialization and transmission
*/

/*
End blob serialization and transmission
*/
int main(void)
{
	png_t *png = malloc(sizeof(*png));
	
	png_open("/ramdisk/test2.png", png);

	rgb_arr_t *arr = malloc(sizeof(*arr));

	printf("conv\n");
	png_conv_rgb(png, arr);
	
	printf("filter\n");
	filter_rgb_value(arr, 90);

	printf("area\n");
	filter_largest_area(arr);

//	int i, j;

	/*for(i = 0; i < IMG_WIDTH; ++i) {
		for(j = 0; j < IMG_HEIGHT; ++j) {
			printf("%d, %d: %d, %d, %d \t", i, j, arr -> px[i][j].r, arr -> px[i][j].g, arr -> px[i][j].b);
		}
		printf("\n");
	}*/

	free(arr);
	png_close(png);

	return 0;
}
