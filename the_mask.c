/*
Copyright © 2015 Kyle Dominguz

This file is part of the_mask.

the_mask is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

the_mask is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with the_mask.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include <sys/resource.h>

#include <linux/videodev2.h>
#include <unistd.h>

#include "the_mask.h"

#define CLEAR(x) memset(&(x), 0, sizeof(x))

/* Function pre-declarations */
int free_ptr(void *alloc);
inline int xioctl(int fh, long int request, void *arg);
int color_comp_thresh(rgb_color_t rgb0, rgb_color_t rgb1, int thresh);
hsv_color_t rgb_to_hsv(rgb_color_t rgb);
inline void mark_int(int x, int y, marker_array_t *marker, int val);
void dfs_comp_mark(short x, short y, int blob_num, rgb_picture_t *pic, \
	marker_array_t *marker);
int dfs_blob_finder(rgb_picture_t *pic, marker_array_t *marker);
void populate_blob_mem(blob_avg_t *blobs, rgb_picture_t *rgb_pic, \
	marker_array_t *marker, int blob_count);
int count_small_blobs(blob_avg_t *blobs, int blob_count);
void filter_small_blobs(blob_avg_t *blobs, blob_avg_t *filtered, \
						int blobCount, int filterCount);
int count_cat_blobs(blob_avg_t *blobs, int blob_count);
void cat_blobs(blob_avg_t *blobs, typed_blob_t *cat_blobs, \
				int blob_count, int catable);
inline int bit8_constrain(double x);
void yuyv_frame_to_rgb(w_buffer_t buf, w_buffer_t *frame);
webcam_t *open_webcam(const char *dev_name);
void close_webcam(webcam_t *w);
void resize_webcam(webcam_t *w, int width, int height);
void read_webcam(webcam_t *w);
void stream_webcam(webcam_t *w, char mode);
void grab_frame(webcam_t *w, w_buffer_t *frame);
void v4l2_frame_to_RGB_pic(w_buffer_t *frame, rgb_picture_t *rgbPic);
int server_connect(int *sockfd, struct sockaddr_in *servaddr);
int set_stack_size(rlim_t size);
void change_cat_blob_order_net(typed_blob_t *source);
unsigned char *serialize_host_cat_blob(typed_blob_t *source, size_t *len);
unsigned char *serialize_data(data_to_send_t *source, size_t *len);
rgb_color_t calc_avg_color(rgb_picture_t *rgb_pic);
int calc_avg_value(rgb_picture_t *rgb_pic);
void adj_container_constraints(rgb_picture_t *rgbPic);
void cleanup();

typedef int32_t index_t;

/* free allocated memory pointed at */
int free_ptr(void *alloc)
{
	if(alloc == NULL) {
		return 1;
	}
	free(alloc);
	return 0;
}

/* run ioctl until it succeeds */
inline int xioctl(int fh, long int request, void *arg)
{
	int r;
	do {
		r = ioctl(fh, request, arg);
	} while(r == -1 && errno == EINTR);

	return r;
}

/* compare two RGB colors based on a treshold using a color difference algoritm */
int color_comp_thresh(rgb_color_t rgb0, rgb_color_t rgb1, int thresh)
{
	int rmean = (rgb0.r + rgb1.r) / 2;
	int r = rgb0.r - rgb1.r;
	int g = rgb0.g - rgb1.g;
	int b = rgb0.b - rgb1.b;
	int diff = sqrt((((512 + rmean) * r * r) >> 8) + 4 * g * g + (((767 - rmean) * b * b) >> 8));
	return diff < thresh;
}

/* convert a RGB color to a HSV color */
hsv_color_t rgb_to_hsv(rgb_color_t rgb)
{
	hsv_color_t returner;

	int min = 0, max = 0, delta = 0;
	int h, s, v;

	rgb.r = rgb.r * 100 / 255;
	rgb.g = rgb.g * 100 / 255;
	rgb.b = rgb.b * 100 / 255;
	
	if(rgb.r >= rgb.g && rgb.r >= rgb.b) {
		max = rgb.r;
		if(rgb.g >= rgb.b)
			min = rgb.b;
		else
			min = rgb.g;
	} else if(rgb.g > rgb.r && rgb.g > rgb.b) {
		max = rgb.g;
		if(rgb.r >= rgb.b)
			min = rgb.b;
		else
			min = rgb.r;
	} else if(rgb.b > rgb.r && rgb.b > rgb.g) {
		max = rgb.b;
		if(rgb.r >= rgb.g)
			min = rgb.g;
		else
			min = rgb.r;
	}
	
	v = max;

	delta = max - min;

	if(max != 0) 
		s = delta * 100 / max;
	else {
		s = 0;
		h = 0;
	}
	
	if(rgb.r == max) {
		if(delta != 0)
			h = ((rgb.g - rgb.b) * 100) / delta;
		else
			h = 0;
	} else if(rgb.g == max) {
		if(delta != 0)
			h = 200 + (((rgb.b - rgb.r) * 100)/ delta);
		else
			h = 0;
	} else {
		if(delta != 0)
			h = 400 + (((rgb.r - rgb.g) * 100) / delta);
		else
			h = 0;
	}

	if(h < 0) {
		h += 360;
	}

	h = h * 6 / 10;

	returner.h = h;
	returner.s = s;
	returner.v = v;

	return returner;
}

/* set the parsed x and y of the marker to the value */
inline void mark_int(int x, int y, marker_array_t *marker, int val)
{
	marker -> cell[x][y] = val;
}

/* recursive implementation of the depth-first search */
void dfs_comp_mark(short x, short y, int blob_num, rgb_picture_t *pic, \
marker_array_t *marker)
{

	mark_int(x, y, marker, blob_num);

	/* if it's at the far left, don't check left */
	if(x > 0) {
		/* check that the value to check is unmarked */
		if(marker -> cell[x - 1][y] == 0) {
			/* compare the current color with the color to the left */
			if(color_comp_thresh((pic -> color[x - 1][y]), (pic -> color[x][y]), THRESH)) {
				/* recursion for the value below */
				dfs_comp_mark(x - 1, y, blob_num, pic, marker);
			}
		}
	}

	/* if it's at the top, don't check up */
	if(y < IMG_HEIGHT - 1) {
		if(marker -> cell[x][y + 1] == 0) {
			if(color_comp_thresh((pic -> color[x][y + 1]), (pic -> color[x][y]), THRESH)) {
				dfs_comp_mark(x, y + 1, blob_num, pic, marker);
			}
		}
	}

	/* if it's at the far right, don't check right */
	if(x < IMG_WIDTH - 1) {
		if(marker -> cell[x + 1][y] == 0) {
			if(color_comp_thresh((pic -> color[x + 1][y]), (pic -> color[x][y]), THRESH)) {
				dfs_comp_mark(x + 1, y, blob_num, pic, marker);
			}
		}
	}

	/* if it's at the far left, don't check left */
	if(y > 0) {
		if(marker -> cell[x][y - 1] == 0) {
			if(color_comp_thresh((pic -> color[x][y - 1]), (pic -> color[x][y]), THRESH)) {
				dfs_comp_mark(x, y - 1, blob_num, pic, marker);
			}
		}
	}
}

/* the base for the recursion of the Depth-first search */
int dfs_blob_finder(rgb_picture_t *pic, marker_array_t *marker)
{	
	int x = 0;
	int y = 0;
	/* stores the final count of blobs */
	int blob_num = 0;

	if(pic == NULL) {
		printf("sumtin done messed up\n");
	}
		for(y = 0; y < IMG_HEIGHT; y++) {
		for(x = 0; x < IMG_WIDTH; x++) {
			/* for all x and y which marker is 0 */
			if(marker -> cell[x][y] == 0){
				/* it's a new blob */
				blob_num++;
				/* begin the recursive DFS */
				dfs_comp_mark(x, y, blob_num, pic, marker);
			}
		}
	}
	/* return the number of blobs */
	return blob_num;
}

/* the method that populates the structure with the blobs */
void populate_blob_mem(blob_avg_t *blobs, rgb_picture_t *rgb_pic, \
				marker_array_t *marker, int blob_count)
{
	/* loop variables */
	index_t i = 0;
	index_t w = 0;
	index_t h = 0;
	/* sets the current blobs' attributes */
	uint8_t r, g, b;
	for(w = 0; w < IMG_WIDTH; w++) {
		for(h = 0; h < IMG_HEIGHT; h++) {
			r = rgb_pic -> color[w][h].r;
			g = rgb_pic -> color[w][h].g;
			b = rgb_pic -> color[w][h].b;

			(blobs + ((marker -> cell[w][h]) - 1)) -> size    += 1;
			(blobs + ((marker -> cell[w][h]) - 1)) -> avg_x   += w;
			(blobs + ((marker -> cell[w][h]) - 1)) -> avg_y   += h;
			((blobs + ((marker -> cell[w][h]) - 1)) -> avg_r) += r;
			((blobs + ((marker -> cell[w][h]) - 1)) -> avg_g) += g;
			((blobs + ((marker -> cell[w][h]) - 1)) -> avg_b) += b;
		}
	}

	/* averages the X, Y, R, G, and B from the size */
	for(i = 0; i < blob_count; i++) {
			(blobs + i) -> avg_x /= (blobs + i) -> size;
			(blobs + i) -> avg_y /= (blobs + i) -> size;
			(blobs + i) -> avg_r /= (blobs + i) -> size;
			(blobs + i) -> avg_g /= (blobs + i) -> size;
			(blobs + i) -> avg_b /= (blobs + i) -> size;
	}

	/* centers the X and Y from the middle rather than the bottom left */
	for(i = 0; i < blob_count; i++) {
		(blobs + i) -> avg_x -= IMG_WIDTH / 2;
		(blobs + i) -> avg_y -= IMG_HEIGHT / 2;
	}
}

int count_small_blobs(blob_avg_t *blobs, int blob_count)
{
	index_t i = 0;
	int filterable = 0;

	for(i = 0; i < blob_count; i++) {
		if((blobs + i) -> size >= BLOB_SIZE_MIN) {
			(blobs + i) -> type = 1;
			filterable++;
		}
	}
	return filterable;
}

/* do some preliminary filtering of the blobs */
void filter_small_blobs(blob_avg_t *blobs, blob_avg_t *filtered, \
						int blobCount, int filterCount)
{
	index_t i = 0;
	int c = 0;
	
	/* for all of the blobs with a type of 1, set them to the blob */
	for(i = 0; i < blobCount; i++){
		if((blobs + i) -> type == 1) {
			(filtered + c) -> avg_x = (blobs + i) -> avg_x;
			(filtered + c) -> avg_y = (blobs + i) -> avg_y;
			(filtered + c) -> size = (blobs + i) -> size;
			(filtered + c) -> avg_r = (blobs + i) -> avg_r;
			(filtered + c) -> avg_g = (blobs + i) -> avg_g;
			(filtered + c) -> avg_b = (blobs + i) -> avg_b;
			(filtered + c) -> type = 0;
			c++;
		}
		if(c > filterCount){
			printf("you done screwed up, son. Aborting to prevent damage.\n");
			break;
		}
	}
}

/* count the number of blobs able to be categorized, and mark their type */
int count_cat_blobs(blob_avg_t *blobs, int blob_count)
{
	index_t i;
	int catCount = 0;

	hsv_color_t hsv;
	rgb_color_t rgb;
	for(i = 0; i < blob_count; i++) {
		rgb.r = (blobs + i) -> avg_r;
		rgb.g = (blobs + i) -> avg_g;
		rgb.b = (blobs + i) -> avg_b;
		hsv = rgb_to_hsv(rgb);
		/* the statement that checks the threshold values */
		if(						\
		hsv.h <= BIN_H_UPPER &&	\
		hsv.h >= BIN_H_LOWER &&	\
		hsv.s <= BIN_S_UPPER &&	\
		hsv.s >= BIN_S_LOWER &&	\
		hsv.v <= BIN_V_UPPER &&	\
		hsv.v >= BIN_V_LOWER)   {
			(blobs + i) -> type = 'c';
			catCount++;
		}
	}
	return catCount;
}

/* categorize the blobs by color */
void cat_blobs(blob_avg_t *blobs, typed_blob_t *cat_blobs, \
				int blob_count, int catable)
{
	index_t i = 0;
	int count = 0;
	
	/* will set all of the blobs with a type that's not 0 to the categorized blobs list */
	for(i = 0, count = 0; i < blob_count; i++) {
		if((blobs + i) -> type != 0){
			(cat_blobs + count) -> type = (blobs + i) -> type;
			(cat_blobs + count) -> size = (blobs + i) -> size;
			(cat_blobs + count) -> avg_x = (blobs + i) -> avg_x;
			(cat_blobs + count) -> avg_y = (blobs + i) -> avg_y;
			count++;
		}
	}

	if(count != catable) {
		printf("Sometin done messd up\n");
	}
}

inline int is_within(int comp, int min, int max) {
	return(comp >= min && comp <= max);
}

int filter_blobs(blobs_t *blobs, filter_t *filter)
{
	index_t i;

	for(i = 0; i < filter blobs -> size; i++) {
		if( \
		is_within(blobs -> blobs -> x_avg, filter -> x_min, filter -> x_max) ||
		is_within(blobs -> blobs -> y_avg, filter -> y_min, filter -> y_max) ||
	}
}

inline int bit8_constrain(double x)
{
	int r = x;
	if(r < 0) {
		return 0;
	}
	else if(r > 255) {
		return 255;
	}
	return r;
}

/* convert a YUYV frame to an RGB frame */
void yuyv_frame_to_rgb(w_buffer_t buf, w_buffer_t *frame)
{
	unsigned int i;
	unsigned char y, u, v;
	int uOffset = 0;
	int vOffset = 0;
	double R, G, B;
	double Y, Pb, Pr;
	
	/* initialize frame */
	if(frame -> start == NULL) {
		frame -> length = buf.length / 2 * 3;
		frame -> start = calloc(frame -> length, sizeof(char));
	}

	/* go through the picture and activley convert the YUYV to RGB */
	for(i = 0; i < buf.length; i += 2) {
		uOffset = (i % 4 == 0) ? 1 : -1;
		vOffset = (i % 4 == 2) ? 1 : -1;

		y = buf.start[i];
		u = (i + uOffset > 0 && i + uOffset < buf.length) ? buf.start[i + uOffset] : 0x80;
		v = (i + vOffset > 0 && i + vOffset < buf.length) ? buf.start[i + vOffset] : 0x80;

		Y  = (255.0 / 219.0) * (y - 0x10);
		Pb = (255.0 / 224.0) * (u - 0x80);
		Pr = (255.0 / 224.0) * (v - 0x80);

		R = 1.0 * Y + 0.000 * Pb + 1.402 * Pr;
		G = 1.0 * Y + 0.344 * Pb - 0.714 * Pr;
		B = 1.0 * Y + 1.772 * Pb + 0.000 * Pr;

		frame -> start[i / 2 * 3 + 0] = bit8_constrain(R);
		frame -> start[i / 2 * 3 + 1] = bit8_constrain(G);
		frame -> start[i / 2 * 3 + 2] = bit8_constrain(B);
	}
}

/* open the webcam */
webcam_t *open_webcam(const char *dev_name)
{
	struct stat st;

	struct v4l2_capability cap;

	int fd;
	webcam_t *w;

	struct v4l2_fmtdesc fmtdesc;
	uint32_t idx = 0;

	if(stat(dev_name, &st) == -1) {
		printf("Cannot identify %s\n", dev_name);
		return NULL;
	}

	fd = open(dev_name, O_RDWR);

	if(fd == -1) {
		printf("Cannot open webcam on %s\n", dev_name);
		return NULL;
	}

	if(xioctl(fd, VIDIOC_QUERYCAP, &cap) == -1) {
		printf("%s is not a valid v4l2 device\n", dev_name);
		return NULL;
	}

	if(!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
		printf("%s is not a capture device\n", dev_name);
		return NULL;
	}

	/* prepare webcam structure */
	w -> fd = fd;
	w -> name = strdup(dev_name);
	w -> frame.start = NULL;
	w -> frame.length = 0;

	/* initialize buffers */
	w -> nbuffers = 0;
	w -> buffers = NULL;
	
	for(;;) {
		fmtdesc.index = idx;
		fmtdesc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

		if(xioctl(w -> fd, VIDIOC_ENUM_FMT, &fmtdesc) == -1)
			break;

		memset(w -> formats[idx], 0, 5);
		memcpy(&w -> formats[idx][0], &fmtdesc.pixelformat, 4);

		idx++;
	}
	return w;
}

/* free the buffers and close a webcam structure */
void close_webcam(webcam_t *w)
{
	size_t i;

	free(w -> frame.start);
	w -> frame.length = 0;

	for(i = 0; i < w -> nbuffers; i++) {
		munmap(w -> buffers[i].start, w -> buffers[i].length);
	}
	
	free(w -> name);
	free(w -> buffers);

	close(w -> fd);
	free(w);
}

/* resize and change the format of a webcam */
void resize_webcam(webcam_t *w, int width, int height)
{
	size_t i;
	struct v4l2_format fmt;
	struct v4l2_buffer buf;
	struct v4l2_requestbuffers req;

	/* use YUYV format */
	CLEAR(fmt);
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	fmt.fmt.pix.width = width;
	fmt.fmt.pix.height = height;
	fmt.fmt.pix.pixelformat = V4L2_PIX_FMT_YUYV;
	fmt.fmt.pix.colorspace = V4L2_COLORSPACE_REC709;

	xioctl(w -> fd, VIDIOC_S_FMT, &fmt);

	/* storing the result */
	w -> width = fmt.fmt.pix.width;
	w -> height = fmt.fmt.pix.height;
	w -> colorspace = fmt.fmt.pix.colorspace;

	if(w -> buffers != NULL) {
		for(i = 0; i < w -> nbuffers; i++) {
			munmap(w -> buffers[i].start, w -> buffers[i].length);
		}

		free(w -> buffers);
	}

	/* request the webcam's buffer for memory mapping */
		CLEAR(req);

	req.count = 4;
	req.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	req.memory = V4L2_MEMORY_MMAP;

	if(xioctl(w -> fd, VIDIOC_REQBUFS, &req) == -1) {
		if(errno == EINVAL) {
			printf("%s does not support memory mapping\n", w -> name);
			return;
		} else {
			printf("Unknown error with VIDIOC_REQBUFS: %d\n", errno);
			return;
		}
	}
	
	/* need at least two buffers */
	if(req.count < 2) {
		printf("Insufficient buffer memory on %s\n", w -> name);
	}

	w -> nbuffers = req.count;
	w -> buffers = calloc(w -> nbuffers, sizeof(w_buffer_t));

	if(!w -> buffers) {
		printf("Out of memory!\n");
		return;
	}

	/* query the buffers on the device */
	for(i = 0; i < w -> nbuffers; i++) {
		CLEAR(buf);
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		buf.index = i;

		if(xioctl(w -> fd, VIDIOC_QUERYBUF, &buf) == -1) {
			printf("Could not query the buffers on %s\n", w -> name);
		}

		w -> buffers[i].length = buf.length;
		w -> buffers[i].start = mmap(NULL, buf.length, PROT_READ | PROT_WRITE, MAP_SHARED, w -> fd, buf.m.offset);

		if(w -> buffers[i].start == MAP_FAILED) {
			printf("Mmap failed!\n");
			return;
		}
	}
}

/* swap buffers with the webcam */
void read_webcam(webcam_t *w)
{
	struct v4l2_buffer buf;
	for(;;) {
		CLEAR(buf);
		buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;

		if(xioctl(w -> fd, VIDIOC_DQBUF, &buf) == -1) {
			switch(errno) {
				case EAGAIN:
					continue;

				case EIO:
				default:
					printf("Could not read from device %s\n", w -> name);
					break;
			}
		}

		yuyv_frame_to_rgb(w -> buffers[buf.index], &w -> frame);
		break;
	}

	if(xioctl(w -> fd, VIDIOC_QBUF, &buf) == -1) {
		printf("Error while swapping buffers on %s\n", w -> name);
		return;
	}
}

/* set the webcam into streaming mode */
void stream_webcam(webcam_t *w, char mode)
{
	size_t i;
	
	struct v4l2_buffer buf;
	enum v4l2_buf_type type;

	if(mode == 1) {
		for(i = 0; i < w -> nbuffers; i++) {
			CLEAR(buf);
			buf.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
			buf.memory = V4L2_MEMORY_MMAP;
			buf.index = i;

			if(xioctl(w -> fd, VIDIOC_QBUF, &buf) == -1) {
				printf("Error clearing buffers on %s\n", w -> name);
				return;
			}
		}

		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if(xioctl(w -> fd, VIDIOC_STREAMON, &type) == -1) {
			printf("Could not turn on streaming on %s\n", w -> name);
			return;
		}
	} else if (mode == 0) {
		type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		if(xioctl(w -> fd, VIDIOC_STREAMOFF, &type) == -1) {
			printf("Could not turn off streaming on %s\n", w -> name);
			return;
		}
	}
}

/* copy the buffer from the webcam to the struct buffer *frame */
void grab_frame(webcam_t *w, w_buffer_t *frame)
{
	/* only copy if there's something in the webcam's framebuffer */
	if(w -> frame.length > 0) {
		if((*frame).start == NULL) {
			(*frame).start = calloc(w -> frame.length, sizeof(char));
			(*frame).length = w -> frame.length;
		}
		
		memcpy((*frame).start, w -> frame.start, w -> frame.length);
	}
}

/* convert a v4l2 buffer to an rgb_picture */
void v4l2_frame_to_RGB_pic(w_buffer_t *frame, rgb_picture_t *rgbPic)
{
	unsigned long int disp = 0;
	int w, h;
	#define R_OFFSET 0
	#define G_OFFSET 1
	#define B_OFFSET 2
	for(h = IMG_HEIGHT - 1; h > -1; h--) {
		for(w = 0; w < IMG_WIDTH; w++) {
			rgbPic -> color[w][h].r = frame -> start[disp + R_OFFSET];
			rgbPic -> color[w][h].g = frame -> start[disp + G_OFFSET];
			rgbPic -> color[w][h].b = frame -> start[disp + B_OFFSET];
			disp += 3;
		}
	}
	#undef R_OFFSET
	#undef G_OFFSET
	#undef B_OFFSET
}

/* connect to the server running on IP_ADDRESS on port PORT */
int server_connect(int *sockfd, struct sockaddr_in *servaddr)
{
	if(sockfd == NULL) {
		return 1;
	}

	if(servaddr == NULL) {
		return 1;
	}

	*sockfd = socket(AF_INET, SOCK_DGRAM, 0);

	bzero(servaddr, sizeof(*servaddr));
	servaddr -> sin_family = AF_INET;
	servaddr -> sin_addr.s_addr = inet_addr(IP_ADDRESS);
	servaddr -> sin_port = htons(PORT);
	return 0;
}

/* set the size of the stack to the value passed */
int set_stack_size(rlim_t size)
{
	struct rlimit old, new;
	int res = 0;

	getrlimit(RLIMIT_STACK, &old);

	if(old.rlim_cur != size)
	{
		new.rlim_cur = size;
		new.rlim_max = size;
		res = setrlimit(RLIMIT_STACK, &new);
		/* if it fails, print the error code and all of the possible error codes */
		if(res == -1){
			printf("Error with rlim: %d. Code IDs: EPERM: %d, EFAULT: %d. EINVAL: %d\n", \
					errno, EPERM, EFAULT, EINVAL);
		}
	}
	return res;
}
/* Convert a cat_blobby to network byte order */
void change_cat_blob_order_net(typed_blob_t *source)
{
	source -> size = htonl(source -> size);
	source -> avg_x = htons(source -> avg_x);
	source -> avg_y = htons(source -> avg_y);
}

/* serialize a cat_blobby and return an unsigned char buffer of bytes */
unsigned char *serialize_host_cat_blob(typed_blob_t *source, size_t *len)
{
	unsigned char *buffer;
	*len = sizeof(*source);
	change_cat_blob_order_net(source);
	buffer = calloc(1, *len);
	memcpy(buffer, source, *len);
	return buffer;
}

/* serialize a data_to_send structure and the content of it's cat_blobby members */
unsigned char *serialize_data(data_to_send_t *source, size_t *len)
{
	int i;
	unsigned char *buffer;
	int count = source -> count;
	size_t blob_size = sizeof(*(source -> blobs));
	*len = 4 * sizeof(unsigned char);
	*len += count * blob_size;
	buffer = calloc(1, *len);
	memcpy(buffer, source, 4);
	for(i = 0; i < count; i++) {
		change_cat_blob_order_net(source -> blobs + i);
		memcpy(buffer + 4 + (blob_size * i), (source -> blobs + i), blob_size);
	}
	return buffer;
}

rgb_color_t calc_avg_color(rgb_picture_t *rgb_pic)
{
	int i, j;
	int count;
	int r = 0;
	int g = 0;
	int b = 0;
	rgb_color_t color;

	for(i = 0; i < IMG_WIDTH; i += 2) {
		for(j = 0; j < IMG_HEIGHT; j += 2) {
			r += rgb_pic -> color[i][j].r;
			g += rgb_pic -> color[i][j].g;
			b += rgb_pic -> color[i][j].b;
		}
	}

	count = (IMG_WIDTH * IMG_HEIGHT) / 4;


	color.r = r / count;
	color.g = r / count;
	color.b = r / count;
	return color;
}

/* returns -1 on fail, should never fail */
int calc_avg_value(rgb_picture_t *rgb_pic)
{
	rgb_color_t color = calc_avg_color(rgb_pic);
	int ret = -1;

	if(color.r >= color.g && color.r >= color.b)
		ret = color.r;
	else if(color.g > color.r && color.g > color.b)
		ret = color.g;
	else if(color.b > color.r && color.b > color.g)
		ret = color.b;

	return ret;
}

void adj_container_constraints(rgb_picture_t *rgbPic)
{
	int value = calc_avg_value(rgbPic);
	printf("value: %d\n", value);
}

void cleanup()
{
	printf("test: \n");
}


int main(void)
{
	int i = 0;	
	uint8_t loop = 1;
	size_t length = 0;
	w_buffer_t frame;

	/* connect to a socked as defined in the header modifying the sockfd and servaddr */
	int sockfd = 0;
	struct sockaddr_in servaddr;

	marker_array_t *marker;
	rgb_picture_t *rgb_pic;

	webcam_t *cam;

	data_to_send_t *data;


	rgb_pic = malloc(sizeof(*rgb_pic));
	marker = malloc(sizeof(*marker));
	
	set_stack_size(104857600);
	atexit(cleanup);

	server_connect(&sockfd, &servaddr);
	cam = open_webcam(WEBCAM_PATH);

	if(cam == NULL) {
		return -1;
	}
	resize_webcam(cam, IMG_WIDTH, IMG_HEIGHT);
	
	stream_webcam(cam, 1);

	do {
		#ifdef WRITE_FILE
			FILE *framestrm;
		#endif
		int blobCount;
		int small_count;
		int catable;
		blob_avg_t *unFilteredBlobs;
		blob_avg_t *filteredBlobs;
		unsigned char *buffer;
		typed_blob_t *catBlobs;

		CLEAR(rgb_pic);
		CLEAR(marker);

		frame.start = NULL;
		frame.length = 0;

		read_webcam(cam);
		/* swap buffers with the webcam */
		for(;;) {
			grab_frame(cam, &frame);
			if(frame.length > 0) {
				break;
			}
		}

		/* if WRITE_FILE is defined, then write the frame grabbed to file */
		#ifdef WRITE_FILE
			framestrm = fopen("frame.rgb", "w+");
			fwrite(frame.start, frame.length, 1, framestrm);
			fclose(framestrm);
		#endif

		/* convert the v4l2 frame to an rgb_picture */
		v4l2_frame_to_RGB_pic(&frame, rgb_pic);

		adj_container_constraints(rgb_pic);
		/* get the blob count from the rgbPic and store blob information in marker */
		blobCount = dfs_blob_finder(rgb_pic, marker);
		/* allocate a new blobby structure and then populate them with the info from the marker */
		unFilteredBlobs = calloc(blobCount, sizeof(*unFilteredBlobs));
		populate_blob_mem(unFilteredBlobs, rgb_pic, marker, blobCount);

		/* count the number of blobs that pass the filter */
		small_count = count_small_blobs(unFilteredBlobs, blobCount);
		/* allocate a new blobby structure and then populate them with the filtered blobs */
		filteredBlobs = calloc(small_count, sizeof(*filteredBlobs));
		filter_small_blobs(unFilteredBlobs, filteredBlobs, blobCount, small_count);
		free_ptr(unFilteredBlobs);

		/* count the number of blobs able to be categorized */
		catable = count_cat_blobs(filteredBlobs, small_count);
		/* allocate a new cat_blobby strucure and then populate them with the categorizable blobs */
		catBlobs = calloc(catable, sizeof(*catBlobs));
		cat_blobs(filteredBlobs, catBlobs, small_count, catable);
		/* clean up a little */
		for(i = 0; i < small_count; i++){
			printf("size: %ld, X: %d, Y: %d, R: %d, G: %d, B: %d", (filteredBlobs + i) -> size, \
				(filteredBlobs + i) -> avg_x, (filteredBlobs + i) -> avg_y, (filteredBlobs + i) -> avg_r, \
				(filteredBlobs + i) -> avg_g, (filteredBlobs + i) -> avg_b);
			printf("\n");
		}
		free_ptr(filteredBlobs);
	
		/* create a new data structure */
		data = calloc(1, sizeof(*data));
		/* set the count to the number of categorizable blobs */
		data -> count = catable;
		/* set the pointer in data to catBlobs */
		data -> blobs = catBlobs;

		printf("catable: %d\n", catable);
		
		for(i = 0; i < catable; i++) {
			printf("size: %ld, x: %d, y: %d", (catBlobs + i) -> size, (catBlobs + i) -> avg_x, (catBlobs + i) -> avg_y);
			printf("\n");
		}

		/* serialize the data and all members into buffer */
		buffer = serialize_data(data, &length);
		printf("----------------------------------------------\n");
	
		/* send the contents of the buffer to the socket declared earlier */
		sendto(sockfd, buffer, length, 0, (struct sockaddr *) &servaddr, sizeof(servaddr));
	
		/* clean up all of the other things */
		free_ptr(catBlobs);
		free_ptr(marker);
		free_ptr(rgb_pic);
		free_ptr(data);
		free_ptr(buffer);
		if(frame.start != NULL) {
			free(frame.start);
		}
	/* stop the looping if loop isn't 1 */
	} while(loop);
	
	stream_webcam(cam, 0);
	close_webcam(cam);

	return 0;
}
